# Kubernetes Monitoring with Prometheus

Monitoring tools to help test the cluster-autoscaler on Kubernetes.

## Usage

```sh
kubectl apply -k https://github.com/prometheus-operator/prometheus-operator\?ref\=v0.51.2
kubectl apply -f https://gitlab.com/WinderAI/tools/manifests/-/raw/main/prometheus-monitoring/manifest.yaml
```
