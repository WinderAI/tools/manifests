# Kubernetes Node Autoscaling Tests

Tools to help when testing the cluster-autoscaler on Kubernetes.

## Usage

```sh
kubectl apply -f https://gitlab.com/WinderAI/tools/manifests/-/raw/main/node-autoscaling/job-scale-cpu.yaml
```
